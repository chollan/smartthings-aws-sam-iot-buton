let response;
const https = require('https');
const querystring = require('querystring');
const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient();
const tableName = process.env.TABLE || 'SmartThingsApps';

exports.lambdaHandler =  (event, context, callback) => {
    if(event.httpMethod == "GET"){
        return callback(null, {
            'statusCode': 200,
            'body': process.env.BUTTONS,
            'headers':{
                'Content-Type': "application/json"
            }
        });
    }else if(event.httpMethod == "POST"){
        console.log(event.body)
        handleButtonUpdate(
            JSON.parse(event.body)
        ).then((data) => {
            return callback(null, {
                'statusCode': 202,
                'body': ''
            });
        }, (err) => {
            return callback(null, {
                'statusCode': 500,
                'body': ''
            });
        });

    }
};

function handleButtonUpdate(data){
    return new Promise((accept, reject) => {
        if("action" in data){
            switch (data.action){
                case "install":
                    appInstall(data).then(accept, reject)
                    break;
                case "update":
                    appUpdate(data).then(accept, reject)
                    break;
                case "uninstall":
                    appUninstall(data).then(accept, reject)
                    break;
                default:
                    reject('missing action');
            }
        }else{
            reject('missing action');
        }

    });
}

function appInstall(body){
    return new Promise((accept, reject) => {
        delete body.action;
        let params = {
            TableName: tableName,
            Item: body
        };

        console.log("Adding a new app record...");
        docClient.put(params, function(err, data) {
            if (err) {
                console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
                return reject(err);
            } else {
                console.log("Added item:", JSON.stringify(data, null, 2));
                return accept(body);
            }
        });
    });
}

function appUpdate(body){
    return new Promise((accept, reject) => {
        let params = {
            TableName:tableName,
            Key:{
                "appId": body.appId
            },
            UpdateExpression: "set button = :b, #token=:t, #url=:u",
            ExpressionAttributeValues:{
                ":b":body.button,
                ":t":body.token,
                ":u":body.url
            },
            ExpressionAttributeNames: {
                '#token': 'token',
                '#url': 'url',
            },
            ReturnValues:"UPDATED_NEW"
        };

        console.log("Updating the item...");
        docClient.update(params, function(err, data) {
            if (err) {
                console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err);
            } else {
                console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
                accept(data);
            }
        });
    });

}

function appUninstall(body){
    return new Promise((accept, reject) => {
        let params = {
            TableName:tableName,
            Key:{
                "appId": body.appId
            }
        };

        console.log("Deleting the item...");
        docClient.delete(params, function(err, data) {
            if (err) {
                console.error("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
                reject(err);
            } else {
                console.log("Delete succeeded:", JSON.stringify(data, null, 2));
                accept(data);
            }
        });
    });
}