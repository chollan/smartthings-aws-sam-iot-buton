const https = require('https');
const querystring = require('querystring');
const async = require('async');
const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient();
const tableName = process.env.TABLE || 'SmartThingsApps';

exports.lambdaHandler =  (event, context, callback) => {
    findRecord(event)
        .then((records) => {
            sendMessagesToSmartThings(records)
                .then((results) => {
                    console.log(results);
                    callback(null, results);
                })
            ;
        }, (err) => {

        })
};

function findRecord(buttonData){
    return new Promise((accept, reject) => {
        let buttonString =  buttonData.serialNumber.replace(/([A-Z0-9]{4})([A-Z0-9]{4})([A-Z0-9]{4})([A-Z0-9]{4})/, "$1-$2-$3-$4");
        var params = {
            TableName: tableName,
            FilterExpression: "#button = :button",
            ExpressionAttributeNames: {
                "#button": "button",
            },
            ExpressionAttributeValues: {
                ":button": buttonString
            }
        };

        console.log("Scanning App Table table.");
        docClient.scan(params, (err, data) => {
            if (err) {
                console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
                reject(err);
            } else {
                // print all the movies
                console.log("Scan succeeded.", data);
                accept({
                    clickType:buttonData.clickType,
                    items:data.Items
                });
            }
        });
    });
}

function sendMessagesToSmartThings(data){
    return new Promise((accept, reject) => {
        let results = [];
        async.each(data.items, (app, callback) => {
            /*let postData = querystring.stringify({
                'type' : data.clickType
            });*/
            let postData = JSON.stringify({
                'type' : data.clickType
            });
            let hostname =  app.url.replace(/https?\:\/\//, "");
            hostname =  hostname.replace(/\:443/, "");
            //console.log(hostname);
            let options = {
                hostname: hostname,
                port: 443,
                path: '/api/smartapps/installations/' + app.appId + '/buttonHandler',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': postData.length,
                    'Authorization': "Bearer "+app.token
                }
            };

            var req = https.request(options, (res) => {
                let data = []
                res.on('data', (d) => {
                    data.push(d);
                    //process.stdout.write(d);
                }).on('end', function() {
                    //at this point data is an array of Buffers
                    //so Buffer.concat() can make us a new Buffer
                    //of all of them together
                    let buffer = Buffer.concat(data);
                    let thisResult = JSON.parse(buffer.toString('utf8'));
                    thisResult.app = app.appId;
                    results.push(
                        thisResult
                    );
                    callback();
                });
            });

            req.on('error', (e) => {
                console.error(e);
            });

            req.write(postData);
            req.end();
        }, (err) => {
            if(err){
                reject(err);
            }else{
                accept(results);
            }
        });
    });
}