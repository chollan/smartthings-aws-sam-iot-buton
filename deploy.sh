#!/usr/bin/env bash
sam package     --template-file template.yaml     --output-template-file packaged.yaml     --s3-bucket ceh-smartthings-sam
aws cloudformation deploy --template-file ./packaged.yaml --stack-name SmartThingsButtonHandler --capabilities CAPABILITY_IAM
